
#include "puzzle.h"
#include <cassert>
#include <string>
#include <sstream>
#include <algorithm> // find


void Puzzle::assert_correct_area()
{
    #ifndef NDEBUG
        // check that the combined sizes of blocks is equal to the size of the puzzle
        int total_area{0};
        for (Block b : blocks)
            total_area += b.get_area();

        assert(total_area == size.get_area() - get_area());
    #endif
}




bool Puzzle::is_solved() const
{
    // assuming legal
    for (const Block& b : blocks) {
        // for more complex puzzles: check for each individual tile
        if (complex) {
            if (overlaps(b))
                return false;
        } else
        // the simple variant: just check if the block is inside the general area
        if (!inside_goal_area(b))
            return false;
    }
    return true;
}

bool Puzzle::inside_goal_area(Block b) const
{
    // assuming b.size is pruned
    return b.offset.x >= 0 && b.offset.y >= 0
    && b.offset.x + b.size.width <= size.width
    && b.offset.y + b.size.height <= size.height;
}


const Block* Puzzle::block_at(const Position& p) const
{
    for (const Block& b : blocks)
        if (b.get(p-b.offset))
            return &b;
    return nullptr;
}

Block* Puzzle::block_at(const Position& p)
{
    for (Block& b : blocks)
        if (b.get(p-b.offset))
            return &b;
    return nullptr;
}

bool Puzzle::move_block_to(Block& block, const Offset& o)
{
    const Offset old_offset{block.offset};
    block.offset = o;
    for (Block& b : blocks) {
        if (&b != &block && b.overlaps(block)) {
            block.offset = old_offset;
            return false;
        }
    }
    return true;
}



void Puzzle::space_blocks()
{
    // TODO: space out blocks so they don't overlap and are not in the goal area
    std::cout << "Puzzle::space_blocks() NYI" << std::endl;
}


static char tile_char = 'O';
static char empty_char = '.';
void Puzzle::draw_ascii(std::ostream& os) const
{
    for (int y = 0; y < size.height; y++) {
        for (int x = 0; x < size.width; x++) {
            Position pos{x,y};

            const Block* bp = block_at(pos);
            if (bp) {
                unsigned index = std::find(blocks.begin(), blocks.end(), *bp) - blocks.begin();
                os << index;
            }
            // if (block_at(pos)) os << tile_char;
            else if (!get(pos)) os << empty_char;
            else os << ' ';
        }
        os << "\n";
    }
}



static const char end_delim = '\n';
std::ostream& operator<<(std::ostream& os, const Puzzle& p)
{
    // no offset info
    os << static_cast<Block>(p) << ' ';
    for (const Block& b : p.blocks)
        os << b << ' ';
    os << end_delim;
    return os;
}

std::istream& operator>>(std::istream& is, Puzzle& p)
{
    if (!(is >> static_cast<Block&>(p))) return is;
    p.blocks.clear();
    std::string data;
    getline(is, data, end_delim);

    std::cout << "Puzzle size: " << (p.size) << ", data: " << data << std::endl;

    std::stringstream ss{data};
    Block b;
    while(ss >> b) {
        b.draw_ascii(std::cout);
        p.blocks.push_back(b);
    }
    return is;
}