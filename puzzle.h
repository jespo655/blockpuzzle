#ifndef PUZZLE_H
#define PUZZLE_H

#include "block.h"
#include <vector>
#include <initializer_list>
#include <iostream>


// A puzzle consists of a number of movable blocks and a static solution block
// The combined area of the movable blocks should be equal to the area of the solution.

// tile in solution = true -> should not be filled
class Puzzle : public Block {
public:
    std::vector<Block> blocks{};
    Puzzle(const Size& size, const std::vector<Block>& blocks) : Block{size}, blocks{blocks} { assert_correct_area(); }
    Puzzle(const Size& size, const std::initializer_list<Block> blocks) : Block{size}, blocks{blocks} { assert_correct_area(); }
    Puzzle(const Size& size, const std::initializer_list<Block> blocks,
        const std::initializer_list<std::vector<bool>>& occupied_tiles) : Block{size, occupied_tiles}, blocks{blocks}, complex{true} { assert_correct_area(); }
    Puzzle() {}     // empty constructor needed for efficient cin>>

    // the puzzle is solved if every tile in the puzzle is covered by a block
    // that is also true if all blocks are in the puzzle at the same time (assuming no blocks can overlap)
    bool is_solved() const;
    Block* block_at(const Position& p);     // nullptr if no block is at the position
    const Block* block_at(const Position& p) const;     // nullptr if no block is at the position
    bool move_block_to(Block& block, const Offset& p);
    bool move_block(Block& block, const Offset& o) { return move_block_to(block, o+block.offset); }
    void draw_ascii(std::ostream&) const override;
    void space_blocks(); // uses space below

private:
    void assert_correct_area();
    bool inside_goal_area(Block b) const;
    bool complex{false};
};

std::ostream& operator<<(std::ostream&, const Puzzle&);
std::istream& operator>>(std::istream&, Puzzle&);

#endif
