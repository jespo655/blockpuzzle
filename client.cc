#include "generator.h"
#include "puzzle.h"
#include "block.h"
#include <iostream>
#include <vector>
#include <cstdlib>

// for keybindings
#include <map>
#include <Windows.h>

// for puzzle files
#include <fstream>
#include <sstream>

#include <algorithm>

class Game_area {

public:
    Size size{20,10};

    Game_area(const Puzzle& p) : puzzle_{p} { set_puzzle_offset(); }    // todo: set size?

    void set_puzzle(const Puzzle& p)
    {
        puzzle_ = p;
        set_puzzle_offset();
        deselect_block();
    }

    friend std::ostream& operator<<(std::ostream& os, const Game_area& ga)
    {
        const Block* selected_block = (ga.selected_block_index>=0 && ga.selected_block_index<ga.puzzle_.blocks.size()) ? &ga.puzzle_.blocks[ga.selected_block_index] : nullptr;
        for (int row = 0; row < ga.size.height; row++) {
            for (int col = 0; col < ga.size.width; col++) {
                Position p{col,row};
                p -= ga.puzzle_.offset;
                const Block* block_at = ga.puzzle_.block_at(p);

                if (selected_block && block_at == selected_block)
                    os << 'X';
                else if (block_at) {
                    unsigned index = std::find(ga.puzzle_.blocks.begin(), ga.puzzle_.blocks.end(), *block_at) - ga.puzzle_.blocks.begin();
                    os << (char)('a'+index);
                    // os << 'O';
                }
                else if (ga.puzzle_.inside(p) && !(ga.puzzle_.get(p)))
                    os << '.';
                else
                    os << ' ';
            }
            os << '\n';
        }
        return os;
    }

    bool has_selected() const { return selected_block_index!=-1; }
    void select_block_index(int i) { selected_block_index = i % puzzle_.blocks.size(); }
    void deselect_block() { selected_block_index = -1; }
    void select_next() { selected_block_index = (selected_block_index+1) % puzzle_.blocks.size(); }
    void select_prev() { selected_block_index = (selected_block_index-1+puzzle_.blocks.size()) % puzzle_.blocks.size(); }
    bool move_selected_block(const Offset offset)
    { return selected_block_index!=-1 && puzzle_.move_block(puzzle_.blocks[selected_block_index],offset); }
    bool is_solved() const { return puzzle_.is_solved(); }

private:
    Puzzle puzzle_;
    int selected_block_index{-1};

    void set_puzzle_offset()
    {
        // assume it fits in the area
        puzzle_.offset.x = (size.width - puzzle_.size.width) / 2;
        puzzle_.offset.y = 1;
    }

};


// https://msdn.microsoft.com/en-us/library/windows/desktop/ms646309%28v=vs.85%29.aspx  // RegisterHotKey(...)
// https://msdn.microsoft.com/en-us/library/windows/desktop/dd375731%28v=vs.85%29.aspx  // list of keys

#define MOD_NOREPEAT 0x4000
struct Keybinding
{
    unsigned short fsModifiers;
    unsigned short vk;
};
static std::map<int,Keybinding> keybinds{};
const static int LEFT = 1;
const static int RIGHT = 2;
const static int UP = 3;
const static int DOWN = 4;
const static int NEXT_BLOCK = 5;
const static int PREV_BLOCK = 6;
const static int EXIT = 7;

bool register_key(int key)
{
    Keybinding& bind = keybinds[key];
    return RegisterHotKey(NULL, key, bind.fsModifiers, bind.vk);
}

bool unregister_key(int key)
{
    return UnregisterHotKey(NULL, key);
}

void register_all()
{
    for (std::pair<int,Keybinding> bind : keybinds)
        RegisterHotKey(NULL, bind.first, bind.second.fsModifiers, bind.second.vk);
}

void unregister_all()
{
    for (std::pair<int,Keybinding> bind : keybinds)
        UnregisterHotKey(NULL, bind.first);
}

void init_keybinds()
{
    keybinds[LEFT] = {MOD_NOREPEAT, VK_LEFT};
    keybinds[RIGHT] = {MOD_NOREPEAT, VK_RIGHT};
    keybinds[UP] = {MOD_NOREPEAT, VK_UP};
    keybinds[DOWN] = {MOD_NOREPEAT, VK_DOWN};
    keybinds[NEXT_BLOCK] = {MOD_NOREPEAT, VK_SPACE};
    keybinds[PREV_BLOCK] = {MOD_NOREPEAT, VK_BACK};
    keybinds[EXIT] = {MOD_NOREPEAT, VK_ESCAPE};
    register_all();
    std::cout << "hotkeys registered" << std::endl;
}




bool handle_keypress(int key, Game_area& ga)
{
    bool changed = false;

    system("cls");
    switch(key) {
        case EXIT:
            throw 0;

        case LEFT:
            std::cout << "LEFT pressed" << std::endl;
            if (ga.has_selected()) {
                for (int d = 1; d<ga.size.width; d++) {
                    if (ga.move_selected_block({-d,0})) {
                        changed = true;
                        break;
                    }
                }
            }
            break;

        case RIGHT:
            std::cout << "LEFT pressed" << std::endl;
            if (ga.has_selected()) {
                for (int d = 1; d<ga.size.width; d++) {
                    if (ga.move_selected_block({d,0})) {
                        changed = true;
                        break;
                    }
                }
            }
            break;

        case UP:
            std::cout << "UP pressed" << std::endl;
            if (ga.has_selected()) {
                for (int d = 1; d<ga.size.width; d++) {
                    if (ga.move_selected_block({0,-d})) {
                        changed = true;
                        break;
                    }
                }
            }
            break;

        case DOWN:
            std::cout << "DOWN pressed" << std::endl;
            if (ga.has_selected()) {
                for (int d = 1; d<ga.size.width; d++) {
                    if (ga.move_selected_block({0,d})) {
                        changed = true;
                        break;
                    }
                }
            }
            break;

        case NEXT_BLOCK:
            ga.select_next();
            changed = true;
            std::cout << "NEXT_BLOCK pressed" << std::endl; break;

        case PREV_BLOCK:
            ga.select_prev();
            changed = true;
            std::cout << "PREV_BLOCK pressed" << std::endl; break;
    }
    std::cout << ga << std::endl;
    return ga.is_solved();
}




void resend_input(int key)
{
    KEYBDINPUT kb = {0};
    INPUT input = {0};
    kb.wVk = keybinds[key].vk;
    input.type = INPUT_KEYBOARD;
    input.ki = kb;

    unregister_key(key);
    SendInput(1, &input, sizeof(input));
    register_key(key);
}


void listen_for_keys(Game_area& ga)
{
    MSG msg = {0};
    while (GetMessage(&msg, NULL, 0, 0) != 0)
    {
        if (msg.message == WM_HOTKEY)
        {
            resend_input(msg.wParam);
            if (handle_keypress(msg.wParam, ga))
                return; // puzzle is solved!
        }
    }
}








static const std::string filename_base = "puzzles";
std::string get_file_name(const Size& size)
{
    std::stringstream ss{};
    ss << filename_base << '_' << size.width << '_' << size.height << ".dat";
    return ss.str();
}

void create_puzzle_file(const Size size)
{
    std::cout << "Generating puzzle file with size " << size.width << "," << size.height << "... ";
    std::vector<Puzzle> puzzles = generate_puzzles_threaded(size, true);
    std::cout << "generated " << puzzles.size() << " puzzles." << std::endl;

    std::ofstream file{get_file_name(size)};
    for (const Puzzle& p : puzzles)
        file << p;
}

std::vector<Puzzle> read_puzzle_file(const Size size)
{
    std::cout << "Reading puzzle file with size " << size.width << "," << size.height << "... " << std::endl;

    std::ifstream file{get_file_name(size)};
    if (!file.is_open()) {
        std::cout << "unable to open file!" << std::endl;
        return {};
    }

    std::vector<Puzzle> puzzles{};
    Puzzle p;
    int i = 0;
    while (file >> p) {
        // std::cout << "reading puzzle " << (++i) << std::endl;
        // std::cout << "Read puzzle " << (++i) << " with pieces:" << std::endl;
        // for (Block& b : p.blocks)
        //     b.draw_ascii(std::cout);

        puzzles.push_back(p);
    }
    std::cout << "read " << puzzles.size() << " puzzles!" << std::endl;
    return puzzles;
}

void generate_puzzle_files()
{
    // create_puzzle_file(Size{2,2});
    // create_puzzle_file(Size{3,2});
    // create_puzzle_file(Size{3,3});
    // create_puzzle_file(Size{4,2});
    // create_puzzle_file(Size{4,3});
    // create_puzzle_file(Size{4,4});
    create_puzzle_file(Size{5,2});
    create_puzzle_file(Size{5,3});
    create_puzzle_file(Size{5,4});
    create_puzzle_file(Size{5,5});
}


#ifndef GENERATOR_MAIN
int main()
{
    // read_puzzle_file(Size{5,2}); return 0;
    // generate_puzzle_files(); return 0;
    std::cout << "Genetating puzzles..." << std::endl;
    std::vector<Puzzle> puzzles = generate_puzzles_threaded(Size{5,2},true);
    std::cout << "Generated " << puzzles.size() << " puzzles!" << std::endl;

    try {
        init_keybinds();
        for (int i = 0; i < puzzles.size(); i++) {
            Game_area ga{puzzles[rand()%puzzles.size()]};
            std::cout << ga << std::endl;
            std::cout << "New Puzzle" << std::endl;
            ga.select_next();
            register_all();
            listen_for_keys(ga);
            unregister_all();
            std::cout << "Puzzle solved! Press any key to continue..." << std::endl;
            system("pause");
            system("cls");
        }
    }
    catch (int) {}
}
#endif

