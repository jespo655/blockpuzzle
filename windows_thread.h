// windows_thread.h
// Gives an easy-to-use abstraction of the windows thread API

#ifndef WINDOWS_THREAD_H
#define WINDOWS_THREAD_H

#include <vector>       // join_multiple with a vector of threads
#include <functional>   // lambdas
#include <Windows.h>    // windows native defines (HANDLE, INFINITE)


class Windows_thread {

public:

    Windows_thread() {}
    Windows_thread(Windows_thread&& other) = default;
    Windows_thread(const Windows_thread&) = delete;
    Windows_thread(const std::function<void()>& fun);

    Windows_thread& operator=(Windows_thread&& other);
    Windows_thread& operator=(const Windows_thread& other) = delete;

    virtual ~Windows_thread();

    void join(int dwMilliseconds = INFINITE);

    bool joinable() const { return thread_id != 0; }
    unsigned get_id() const { return thread_id; }
    HANDLE native_handle() const { return handle; }
    static unsigned hardware_concurrency() { return 8; }
    void detatch();
    void swap(Windows_thread& other);

private:
    bool started = false;
    HANDLE handle = 0;
    unsigned thread_id = 0;  // unsigned long int
    std::function<void()> function;     // storing the function in the thread object ensures lambdas won't run out of scope

protected:
    void start(unsigned (*fun)(void*), void* arg = nullptr);

};


// waits for one thread.
void join(Windows_thread& thread, int dwMilliseconds = INFINITE);


// waits for multiple threads.
// If wait_all is false, then the function will return when the first thread is finished.
void join_multiple(int count, const Windows_thread* threads, bool wait_all = true, int dwMilliseconds = INFINITE);
void join_multiple(const std::vector<Windows_thread> threads, bool wait_all, int dwMilliseconds);



#endif