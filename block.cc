#include "block.h"
#include <array>
#include <string>
#include <cmath>

// TODO: prune size
Block::Block(const Size& size, const std::initializer_list<std::vector<bool>>& initializer_list)
	: size{size}, data_(size.get_area())
{
	if (size.get_area() == 0) return;
	int y = 0;
	for (std::vector<bool> v : initializer_list) {
		for (int x = 0; x < v.size() && x < size.width; x++) {
			set(x,y,v[x]);
		}
		if (++y >= size.height) return;
	}
}

Block Block::rotate_counter_clockwise()
{
	Block block{Size{size.height,size.width}};
	for (int x = 0; x < size.width; x++)
		for (int y = 0; y < size.height; y++)
			block.set(y,size.width-x-1,get_unchecked(x,y));
	return block;
}

Block Block::rotate_clockwise()
{
	Block block{Size{size.height,size.width}};
	for (int x = 0; x < size.width; x++)
		for (int y = 0; y < size.height; y++)
			block.set(size.height-y-1,x,get_unchecked(x,y));
	return block;
}

Block Block::rotate_180()
{
	Block block{size};
	for (int x = 0; x < size.width; x++)
		for (int y = 0; y < size.height; y++)
			block.set(size.width-x-1,size.height-y-1,get_unchecked(x,y));
	return block;
}

Block Block::mirror_horizontal()
{
	Block block{size};
	for (int x = 0; x < size.width; x++)
		for (int y = 0; y < size.height; y++)
			block.set(x,size.height-y-1,get_unchecked(x,y));
	return block;
}

Block Block::mirror_vertical()
{
	Block block{size};
	for (int x = 0; x < size.width; x++)
		for (int y = 0; y < size.height; y++)
			block.set(size.width-x-1,y,get_unchecked(x,y));
	return block;
}



unsigned Block::get_area() const
{
	unsigned area{0};
	for (const Position& p : *this)
		if (get_unchecked(p))
			area++;
	return area;
}



// OVERLAPS IS THE BIGGEST CPU HOG FOR PUZZLE GENERATION
// OPTIMIZE THIS
bool Block::overlaps(const Block& b) const
{

	const int offset_dx = b.offset.x-offset.x;
	const int offset_dy = b.offset.y-offset.y;

	if ((int)b.size.width+offset_dx < 0 || (int)b.size.height+offset_dy < 0)
		return false;

	const size_t min_x = std::max(0,offset_dx);
	const size_t min_y = std::max(0,offset_dy);

	const size_t max_x = std::min(size.width, b.size.width+offset_dx);
	const size_t max_y = std::min(size.height, b.size.height+offset_dy);

	for (size_t x = min_x; x < max_x; x++)
		for (size_t y = min_y; y < max_y; y++)
			if (get_unchecked(x,y) && b.get_unchecked(x-offset_dx,y-offset_dy))
				return true;

	return false;

}


// static char tile_char = (char)219;
static const char tile_char = 'O';
void Block::draw_ascii(std::ostream& os) const
{
	os << "block with size w=" << size.width << " h=" << size.height << " x=" << offset.x << " y=" << offset.y << "\n";
	for (int y = 0; y < size.height; y++) {
		for (int x = 0; x < size.width; x++) {
			os << (get_unchecked(x,y) ? tile_char : ' ');
		}
		os << "\n";
	}
}


// TODO: print short string
static const char block_char = 'T';
static const char empty_char = 'F';
std::ostream& operator<<(std::ostream& os, const Block& block)
{
	// w h data
	// 2 3 TTFTFT
	os << block.size.width << ' ' << block.size.height << ' ';
	for (const Position& p : block)
		os << (block.get(p) ? block_char : empty_char);
	return os;
}


std::istream& operator>>(std::istream& is, Block& block)
{
	unsigned w, h;
	std::string data;
	if (!(is >> w >> h >> data)) return is;
	if (data.size() != w*h) {
		std::cout << "Failed to read block - malformed input" << std::endl;
		is.setstate(is.failbit);
		return is;
	}

	block = Block(Size{w,h});

	size_t i = 0;
	for (const Position& p : block)
		if (data[i++] == block_char)
			block.set(p, true);
	return is;
}








// block_of_size_n gives a vector containin all legal blocks of that size.
// A legal block of size n consists of n tiles that are all adjacent.
// No "floating" pieces.

// TODO: generate these algorithmically for a general n.
std::vector<Block> blocks_of_size_1()
{
	// 1
    return {Block{Size{1,1},{{true}}}};
}

std::vector<Block> blocks_of_size_2()
{
	// 2
    Block block{Size{2,1},{{true, true}}};
    return {block, block.rotate_clockwise()};
}

std::vector<Block> blocks_of_size_3()
{
	// 6
    Block straight{Size{3,1},{{true, true, true}}};
    Block curve{Size{2,2},{{true, true}, {true, false}}};
    return {straight, straight.rotate_clockwise(),
        curve, curve.rotate_clockwise(), curve.rotate_counter_clockwise(), curve.rotate_180()};
}

std::vector<Block> blocks_of_size_4()
{
/*
    ##  ####    ##       ##     ###     ###     ###
    ##           ##     ##       #      #         #
*/
	// 19
    Block square{Size{2,2}, {{true, true}, {true, true}}};
    Block straight{Size{4,1}, {{true, true, true, true, true}}};
    Block Z{Size{3,2}, {{true, true, false}, {false, true, true}}};
    Block Z2{Z.mirror_vertical()};
    Block T{Size{3,2}, {{true, true, true}, {false, true, false}}};
    Block L{Size{2,3}, {{true, false}, {true, false}, {true, true}}};
    Block J{Size{2,3}, {{false, true}, {false, true}, {true, true}}};
    return {square, straight, straight.rotate_clockwise(),
        Z, Z.rotate_clockwise(),
        Z2, Z2.rotate_clockwise(),
        T, T.rotate_clockwise(), T.rotate_counter_clockwise(), T.rotate_180(),
        L, L.rotate_clockwise(), L.rotate_counter_clockwise(), L.rotate_180(),
        J, J.rotate_clockwise(), J.rotate_counter_clockwise(), J.rotate_180()};
}

std::vector<Block> blocks_of_size_5()
{
/*
    #####   ####   ###    ###     ##    ##      ##  ##      ###     ##    ##   ##
            #      #        ##     ##  ##       ##  ##       #       #    #     #
                   #               #    #       #    #       #       ##  ##    ##
            ####            ##
               #          ###

     ##     ####    ####
    ##       #        #
    #

*/
	// 62
    Block straight{Size{5,1},{{true, true, true, true, true}}};
    Block plus{Size{3,3},{{false, true},{true, true, true},{false, true}}};
    Block L1{Size{4,2},{{true, true, true, true}, {true, false, false, false}}};
    Block J{L1.mirror_horizontal()};
    Block L2{Size{3,3},{{true, true, true}, {true, false, false}, {true, false, false}}};
    Block Z1{Size{4,2},{{true, true, true, false}, {false, false, true, true}}};
    Block Z1m{Z1.mirror_horizontal()};
    Block weird_1{Size{3,3},{{true, true}, {false, true, true}, {false, true}}};
    Block weird_2{weird_1.mirror_horizontal()};
    Block dumpling_1{Size{3,2},{{true,true,true},{true,true}}};
    Block dumpling_2{dumpling_1.mirror_horizontal()};
    Block T{Size{3,3},{{true,true,true},{false,true},{false,true}}};
    Block Z2{Size{3,3},{{true,true},{false,true},{false,true,true}}};
    Block Z2m{Z2.mirror_horizontal()};
    Block C{Size{3,2},{{true,true,true},{true,false,true}}};
    Block W{Size{3,3},{{false, true, true}, {true, true}, {true}}};
    Block T2{Size{4,2},{{true,true,true,true},{false,true}}};
    Block T2m{T2.mirror_horizontal()};
    return {straight, straight.rotate_clockwise(), plus,
        L1, L1.rotate_clockwise(), L1.rotate_counter_clockwise(), L1.rotate_180(),
        J, J.rotate_clockwise(), J.rotate_counter_clockwise(), J.rotate_180(),
        L2, L2.rotate_clockwise(), L2.rotate_counter_clockwise(), L2.rotate_180(),
        Z1, Z1.rotate_clockwise(), Z1.rotate_counter_clockwise(), Z1.rotate_180(),
        Z1m, Z1m.rotate_clockwise(), Z1m.rotate_counter_clockwise(), Z1m.rotate_180(),
        weird_1, weird_1.rotate_clockwise(), weird_1.rotate_counter_clockwise(), weird_1.rotate_180(),
        weird_2, weird_2.rotate_clockwise(), weird_2.rotate_counter_clockwise(), weird_2.rotate_180(),
        dumpling_1, dumpling_1.rotate_clockwise(), dumpling_1.rotate_counter_clockwise(), dumpling_1.rotate_180(),
        dumpling_2, dumpling_2.rotate_clockwise(), dumpling_2.rotate_counter_clockwise(), dumpling_2.rotate_180(),
        T, T.rotate_clockwise(), T.rotate_counter_clockwise(), T.rotate_180(),
        Z2, Z2.rotate_clockwise(),
        Z2m, Z2m.rotate_clockwise(),
        C, C.rotate_clockwise(), C.rotate_counter_clockwise(), C.rotate_180(),
        W, W.rotate_clockwise(), W.rotate_counter_clockwise(), W.rotate_180(),
        T2, T2.rotate_clockwise(), T2.rotate_counter_clockwise(), T2.rotate_180(),
        T2m, T2m.rotate_clockwise(), T2m.rotate_counter_clockwise(), T2m.rotate_180()};
}

static const int size = 5;
static std::array<std::vector<Block>,size> cached_blocks{};
static std::array<bool,size> calculated{};

std::vector<Block> blocks_of_size(int size)
{
	if (size<1 || size>5) return {};
	if (calculated[size-1])
		return cached_blocks[size-1];

	std::vector<Block> blocks;
	switch(size) {
		case 1: blocks = blocks_of_size_1(); break;
		case 2: blocks = blocks_of_size_2(); break;
		case 3: blocks = blocks_of_size_3(); break;
		case 4: blocks = blocks_of_size_4(); break;
		case 5: blocks = blocks_of_size_5(); break;
	}
	cached_blocks[size-1] = blocks;
	calculated[size-1] = true;
	return blocks;
}

int number_of_blocks_of_size(int size)
{
	return blocks_of_size(size).size();
}




