#ifndef GENERATOR_H
#define GENERATOR_H

#include <vector>

class Puzzle;
class Size;

std::vector<Puzzle> generate_puzzles(const Size& size, bool debug_prints = false);
std::vector<Puzzle> generate_puzzles_threaded(const Size& size, bool debug_prints = false);

// #define GENERATOR_MAIN

#endif