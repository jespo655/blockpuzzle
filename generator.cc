#include "generator.h"
#include "block.h"
#include "puzzle.h"
#include "windows_thread.h"

#include <iostream>
#include <vector>
#include <initializer_list>
#include <array>
#include <cmath>
#include <algorithm>

#include <iomanip>

/*
Generator:

Find setups with unique solutions

Algorithm:
1) Generate a block of a certain size
2) Generate random blocks of different kinds, so that their total area equals the size from 1)
3) Depth first search:
	a) for blocks in order of width*height:
	b) iterate for all possible positions, place it there, then go to the next block
	c) no possible positions -> backtrack
	d) last block placed -> solution found. Save it temporary
	e) another solution found -> discard the solution and go back to 2
	f) no other solution found -> save the temporary solution to the list of puzzles.

*/












static const int MIN_SIZE = 1;
static const int MAX_SIZE = 5;





template<int min, int max>
class Numbers {
public:
    auto begin() const { return amount.begin(); }
    auto end() const { return amount.end(); }
    int& operator[](int i) { return amount[i-min]; }
    int operator[](int i) const { return amount[i-min]; }

    bool operator==(const Numbers& other) const { return amount == other.amount; }
    bool operator!=(const Numbers& that) const { return !(*this == that); }

    template<typename... T>
    Numbers(T... ts) : amount{ts...} {}

    int sum()
    {
        int s;
        for (int i = min; i <= max; i++)
            s+=i*amount[i];
        return s;
    }

private:
    std::array<int,max-min+1> amount{};
};


template<int min, int max>
std::ostream& operator<<(std::ostream& os, const Numbers<min,max>& ns)
{
    for (int n : ns)
        os << n << " ";
    return os;
}





// http://stackoverflow.com/questions/9330915/number-of-combinations-n-choose-r-in-c
unsigned nChoosek( unsigned n, unsigned k )
{
    if (k > n) return 0;
    if (k * 2 > n) k = n-k;
    if (k == 0) return 1;

    int result = n;
    for( int i = 2; i <= k; ++i ) {
        result *= (n-i+1);
        result /= i;
    }
    return result;
}









template<int min, int max>
void number_combinations_recursive(int remaining, std::vector<Numbers<min,max>>& combinations, Numbers<min,max>& numbers, int next_to_set = max)
{
    if (remaining == 0) {
        combinations.push_back(numbers);
        return;
    }

    if (next_to_set < min) return;

    for (int i = std::min(remaining / next_to_set, number_of_blocks_of_size(next_to_set)); i >= 0; i--) {
        numbers[next_to_set] = i;
        number_combinations_recursive(remaining - i*next_to_set, combinations, numbers, next_to_set-1);
    }
}

template<int min, int max>
std::vector<Numbers<min,max>> number_combinations(int sum)
{
    std::vector<Numbers<min,max>> combinations{};
    combinations.reserve(std::min(sum*2,168));      // 168 hard coded max, from tests with min=1,max=5
    Numbers<min,max> number{};
    number_combinations_recursive(sum, combinations, number);
    combinations.shrink_to_fit();
    return combinations;
}



template<int min, int max>
void block_combinations_recursive(Numbers<min,max>& remaining, Numbers<min,max>& index, std::vector<Block>& blocks, std::vector<std::vector<Block>>& combinations, const Size& max_size)
{
    // find a size that should be added
    int size = min;
    while(remaining[size] == 0 && size <= max)
        size++;
    if (remaining[size] == 0) {
        // No blocks left to add. Push the combination and backtrack.
        combinations.push_back(blocks);
        return;
    }

    remaining[size]--;
    int old_i = index[size];

    // find a new block to use
    std::vector<Block> possible_blocks = blocks_of_size(size);
    for (int i = index[size]; i < possible_blocks.size(); i++) {
        Block& b = possible_blocks[i];
        if (b.size.width > max_size.width || b.size.height > max_size.height) continue;
        bool block_used = false;
        for (const Block& b2 : blocks) {
            if (&b == &b2) {
                block_used = true;
                break;
            }
        }
        if (!block_used) {
            // found a legal block -> add it to a new vector and continue recursively.
            // After, try to find new blocks and continue until no more blocks can be found.
            blocks.push_back(b);
            index[size] = i+1;
            block_combinations_recursive(remaining, index, blocks, combinations, max_size);
            blocks.pop_back();
        }
    }
    index[size] = old_i;
    remaining[size]++;
}

template<int min, int max>
std::vector<std::vector<Block>> block_combinations(Numbers<min,max> numbers, const Size& max_size)
{
    unsigned permutations{1};
    unsigned sum{};
    for (int i = min; i <= max; i++) {
        permutations *= nChoosek(number_of_blocks_of_size(i),numbers[i]);    // combination: n out of blocks_of_size(i)
        sum += numbers[i];
    }
    Numbers<min,max> index{};
    std::vector<Block> blocks{};
    std::vector<std::vector<Block>> combinations{};
    blocks.reserve(sum);
    combinations.reserve(permutations);

    block_combinations_recursive(numbers, index, blocks, combinations, max_size);
    combinations.shrink_to_fit();
    return combinations;
}




// search through all permutations.
// assumes block size is pruned
int number_of_solutions(Puzzle& p, const int max_solutions = 1, const int block_to_place_index = 0)
{
    if (block_to_place_index == p.blocks.size()) {
        return 1; // no more blocks to place -> solved! (checking p.is_solved() is unnecessary)
    }

    Block& block_to_place = p.blocks[block_to_place_index];

    int sum = 0;
    for (int x = 0; x <= p.size.width-block_to_place.size.width; x++) {
        for (int y = 0; y <= p.size.height-block_to_place.size.height; y++) {
            if (p.move_block_to(block_to_place, Offset(x,y))) {
                sum += (number_of_solutions(p,max_solutions,block_to_place_index+1));
                if (sum > max_solutions) {
                    block_to_place.offset.x= -(p.size.width+1);  // move the block out of the way before backtracking to avoid bad collisions
                    return sum;
                }
            }
        }
    }

    block_to_place.offset.x= -(p.size.width+1);  // move the block out of the way before backtracking to avoid bad collisions
    return sum;
}

std::vector<Puzzle> generate_puzzles(const Size& size, bool print)
{
    // for each possible number combination, make a block combination and try to generate puzzles for each combination.
    // Each block combination can result in a puzzle.
    std::vector<Puzzle> one_solution_puzzles{};

    std::vector<Numbers<MIN_SIZE,MAX_SIZE>> n_combs = number_combinations<MIN_SIZE,MAX_SIZE>(size.get_area());
    int n = n_combs.size(); // debug
    for (auto ns : n_combs) {
        std::vector<std::vector<Block>> b_combs = block_combinations(ns, size);

        // reserve the max size to avoid lots and lots of vector moves
        // this will allocate much more memory than needed, but that's ok because it saves computation time
        one_solution_puzzles.reserve(one_solution_puzzles.size() + b_combs.size() * (n--));

        if (print) std::cout << "blocks: " << ns << ", combinations: " << b_combs.size();
        int solutions = one_solution_puzzles.size();

        for (std::vector<Block> blocks : b_combs) {

            Puzzle p{size, blocks}; // generate the initial puzzle
            for (Block& b : p.blocks)
                b.offset.x = -(b.size.width+1); // move all blocks outside the goal area

            if (number_of_solutions(p,1) == 1)
                one_solution_puzzles.push_back(p);

        }
        if (print) std::cout << ", puzzles: " << one_solution_puzzles.size() - solutions << std::endl;
    }

    one_solution_puzzles.shrink_to_fit();
    if (print) std::cout << "Generation finished. Puzzles generated = " << one_solution_puzzles.size() << std::endl;
    return one_solution_puzzles;
}


std::vector<Puzzle> generate_puzzles_threaded_old(const Size& size, bool print)
{
    // generate number och block combinations normally,
    // then for each block combination, start a new thread
    // store the results in a array and append them all when done.
    std::vector<Numbers<MIN_SIZE,MAX_SIZE>> n_combs = number_combinations<MIN_SIZE,MAX_SIZE>(size.get_area());

    const int n = n_combs.size();
    std::vector<std::vector<Block>> b_combs[n];
    Windows_thread threads[n];

    if (print) std::cout << "Calculating block combinations with " << n << " threads..." << std::endl;
    // calculate block combinations
    for (int i = 0; i < n; i++) {
        threads[i] = Windows_thread([&, i]()
        {
            b_combs[i] = block_combinations(n_combs[i],size);
            if (print) std::cout << "Thread " << i << " finished. Ns = " << n_combs[i] << ", Cs = " << b_combs[i].size() << std::endl;
        });
    }
    join_multiple(n,threads);

    if (print) for (int i = 0; i < n; i++) std::cout << "Thread " << i << ": " << n_combs[i] << ", Combinations generated = " << b_combs[i].size() << std::endl;

    if (print) std::cout << "Block combinations done, generating puzzles..." << std::endl;
    // calculate puzzles
    std::vector<Puzzle> puzzles[n];
    for (int i = 0; i < n; i++) {
        threads[i] = Windows_thread([&, i]()
        {
            int goal = 1000;
            puzzles[i].reserve(b_combs[i].size()/50);
            for (std::vector<Block> blocks : b_combs[i]) {

                Puzzle p{size, blocks}; // generate the initial puzzle
                for (Block& b : p.blocks)
                    b.offset.x = -(b.size.width+1); // move all blocks outside the goal area

                if (number_of_solutions(p,1) == 1)
                    puzzles[i].push_back(p);

                if (puzzles[i].size() == goal) {
                    goal+=500;
                    std::cout << "Thread " << i << " reached " << puzzles[i].size() << " puzzles. Continuing..." << std::endl;
                }
            }
            puzzles[i].shrink_to_fit();
            if (print) std::cout << "Thread " << i << " finished. Ns = " << n_combs[i] << ", Cs = " << b_combs[i].size() << ", Puzzles = " << puzzles[i].size() << std::endl;
        });
    }
    join_multiple(n,threads);

    int total_puzzles = 0;
    for (int i = 0; i < n; i++)
        total_puzzles += puzzles[i].size();

    std::vector<Puzzle> puzzle_vector{};
    puzzle_vector.reserve(total_puzzles);
    for (std::vector<Puzzle>& p : puzzles)
        puzzle_vector.insert(puzzle_vector.end(), p.begin(), p.end());

    if (print) std::cout << "Generation finished. Puzzles generated = " << total_puzzles << std::endl;

    return puzzle_vector;
}







std::vector<Puzzle> generate_puzzles_threaded(const Size& size, bool print)
{
    // return generate_puzzles_threaded_old(size,print);
    // generate number och block combinations normally,
    // then for each block combination, start a new thread
    // store the results in a array and append them all when done.
    std::vector<Numbers<MIN_SIZE,MAX_SIZE>> n_combs = number_combinations<MIN_SIZE,MAX_SIZE>(size.get_area());

    const int n = n_combs.size();
    Windows_thread threads[n];
    std::vector<Puzzle> puzzles[n];

    if (print) std::cout << "Starting " << n << " threads..." << std::endl;
    for (int i = 0; i < n; i++) {
        threads[i] = Windows_thread([&n_combs, &puzzles, &size, print, i]()
        {
            // calculate block combinations
            std::vector<std::vector<Block>> b_combs = block_combinations(n_combs[i],size);
            if (print) std::cout << "Thread " << i << " Ns = " << n_combs[i] << ", Cs = " << b_combs.size() << std::endl;

            // calculate puzzles
            int goal = 1000;
            for (std::vector<Block> blocks : b_combs) {

                Puzzle p{size, blocks}; // generate the initial puzzle
                for (Block& b : p.blocks)
                    b.offset.x = -(b.size.width+1); // move all blocks outside the goal area

                if (number_of_solutions(p,1) == 1)
                    puzzles[i].push_back(p);

                if (print && puzzles[i].size() == goal) {   // debug prints
                    goal+=500;
                    std::cout << "Thread " << i << " reached " << puzzles[i].size() << " puzzles. Continuing..." << std::endl;
                }
            }

            if (print) std::cout << "Thread " << i << " Ns = " << n_combs[i] << ", Cs = " << b_combs.size() << ", Ps = " << puzzles[i].size() << std::endl;
        });
    }
    join_multiple(n,threads);

    int total_puzzles = 0;
    for (int i = 0; i < n; i++)
        total_puzzles += puzzles[i].size();

    std::vector<Puzzle> puzzle_vector{};
    puzzle_vector.reserve(total_puzzles);
    for (std::vector<Puzzle>& p : puzzles)
        puzzle_vector.insert(puzzle_vector.end(), p.begin(), p.end());

    if (print) std::cout << "Generation finished. Puzzles generated = " << total_puzzles << std::endl;

    return puzzle_vector;
}

























/*
// To generate a puzzle:

Start with a size
Generate a number combination with the same sum as the total area (several combinations possible)
Generate a combination of blocks of those sizes (several combinations possible)
Check if the blocks fit in the puzzle with exactly one solution (true or false)
If there is exactly one solution, add the puzzle to the list OR print it to a file (synchronization needed?)

*/



template<int min, int max>
class Numbers_iterator {

public:
    Numbers<min,max> operator*() const { return numbers_; }
    bool operator==(const Numbers_iterator& other) const { return numbers_ == other.numbers_; }
    bool operator!=(const Numbers_iterator& that) const { return !(*this == that); }

    static Numbers_iterator begin(int sum) { return Numbers_iterator(sum); }
    static Numbers_iterator end() { Numbers_iterator n{0}; n.next_to_change_ = max+1; return n; }

    Numbers_iterator(int sum) : sum_{sum}, numbers_{}, max_numbers_{}, next_to_change_{min}
    {
        for (int i = min; i <= max; i++)
            max_numbers_[i] = std::min(sum/i, number_of_blocks_of_size(i));

        numbers_ = max_numbers_;
        if (numbers_.sum() != sum_)
            (*this)++;
    }

    Numbers_iterator(const Numbers_iterator& other) = default;

    Numbers_iterator operator++(int)
    {
        // suffix
        Numbers_iterator it{*this};
        operator++();
        return it;
    };

    Numbers_iterator& operator++()
    {
        while(true) {
            while(next_to_change_ <= max && numbers_[next_to_change_] == 0)
                next_to_change_++;

            if (next_to_change_ > max)
                return *this; // should now be == end

            numbers_[next_to_change_]--; // decrement

            if (fill_big(next_to_change_-1))
                return *this; // should not be == end
        }
    }

private:
    Numbers<min,max> numbers_{};
    Numbers<min,max> max_numbers_{};
    int sum_;
    int next_to_change_ = min;

    // Goes from start to min and fills with as big numbers as possible
    // returns false if failed to fill up exactly
    bool fill_big(int start = max)
    {
        int remaining = sum_;
        // subract the sum of the numbers that shouldn't be changed
        for (int i = max; i > start; i--)
            remaining -= numbers_[i] * i;

        next_to_change_ = min; // should always point to the most recently changed element

        if (remaining < 0) return false;

        for (int i = start; i >= min; i--) {
            numbers_[i] = std::min(remaining/i, max_numbers_[i]);
            remaining -= numbers_[i]*i;
            if (remaining == 0) return true;
        }
        return remaining == 0;
    }
};

















// // http://stackoverflow.com/questions/9330915/number-of-combinations-n-choose-r-in-c
// unsigned nChoosek( unsigned n, unsigned k )
// {
//     if (k > n) return 0;
//     if (k * 2 > n) k = n-k;
//     if (k == 0) return 1;

//     int result = n;
//     for( int i = 2; i <= k; ++i ) {
//         result *= (n-i+1);
//         result /= i;
//     }
//     return result;
// }


    // for (int i = min; i <= max; i++) {
    //     combinations *= nChoosek(number_of_blocks_of_size(i),numbers[i]);    // combination: n out of blocks_of_size(i)
    //     sum += numbers[i];
    // }
    // std::cout << "blocks: " << sum << ", combinations: " << combinations << std::endl:







template<int min, int max>
class Block_combination_iterator {

public:

    bool operator==(const Block_combination_iterator& other) const
    {
        if (end_ || other.end_) return end_ && other.end_;
        else return chosen_ == other.chosen_;
    }
    bool operator!=(const Block_combination_iterator& that) const { return !(*this == that); }

    Block_combination_iterator operator++(int)
    {
        // suffix
        Block_combination_iterator it{*this};
        operator++();
        return it;
    };

    Block_combination_iterator(Numbers<min,max> ns) : chosen_{}, sum{ns.sum()}
    {
        for (int n = 0; n < max-min+1; ++n) {
            auto& v = chosen_[n];
            v.resize(ns[n+min]);
            for (int i = 0; i < v.size(); ++i) {
                v[i] = i;
            }
        }
    }

    static Block_combination_iterator begin(Numbers<min,max> ns) { return Block_combination_iterator(ns); }
    static Block_combination_iterator end(Numbers<min,max> ns) { auto it = begin(ns); it.end_ = true; return it; }

    Block_combination_iterator& operator++()
    {
        if (end_) return *this;

        // increment the last value in a list
        // the max value = number_of_blocks(n)
        // if hitting the max, go back to the previous value and increment that, then put the remaining values incrementing after it
        // that value (v[i]) cannot be bigger than v[i+1]
        // if v[0] cannot be incremented, put it back to the init values and go to the next vector
        // can't increment the v_n[0] -> end

        int size = min-1;
        for (auto& v : chosen_) {
            size++;
            if (v.empty()) continue;

            int i = v.size()-1; // which number to increment
            int max_b = number_of_blocks_of_size(size);

            // find the last element in the list that we can legally increment
            while ((i == v.size()-1 && v[i] == max_b-1)
                || (i < v.size()-1 && v[i] == v[i+1]-1)) {
                if (--i < 0) {
                    break;
                }
            }

            if (i < 0) {
                // can't move any element in the list

                // reset the list
                for (int a = 0; a < v.size(); ++a)
                    v[a] = a;

                // go to the next list
                continue;
            }

            // move the elements accordingly and return
            int b = v[i];
            for (; i < v.size(); ++i)
                v[i] = ++b;
            return *this;
        }
        end_ = true;
    }

    std::vector<Block> operator*() const
    {
        std::vector<Block> blocks{};
        blocks.reserve(sum);
        for (int size = min; size <= max; size++) {
            auto v = chosen_[size-min];
            auto all_blocks = blocks_of_size(size);
            for (int i = 0; i < v.size(); ++i) {
                if (v[i]) {
                    blocks.push_back(all_blocks[i]);
                }
            }
        }
        return blocks;
    }



private:
    // the array contains one vector for each size
    // the vector should always contain unique values
    // constant size = number of blocks to choose
    // start: incrementing values 0 to n
    std::array<std::vector<int>,max-min+1> chosen_{};
    bool end_ = false;
    const int sum;

};
































// specify begin and end with the default min and max sizes
Numbers_iterator<MIN_SIZE,MAX_SIZE> begin(Size size) { return Numbers_iterator<MIN_SIZE,MAX_SIZE>::begin(size.get_area()); }
Numbers_iterator<MIN_SIZE,MAX_SIZE> end(Size) { return Numbers_iterator<MIN_SIZE,MAX_SIZE>::end(); }

Block_combination_iterator<MIN_SIZE,MAX_SIZE> begin(Numbers<MIN_SIZE,MAX_SIZE> ns) { return Block_combination_iterator<MIN_SIZE,MAX_SIZE>::begin(ns); }
Block_combination_iterator<MIN_SIZE,MAX_SIZE> end(Numbers<MIN_SIZE,MAX_SIZE> ns) { return Block_combination_iterator<MIN_SIZE,MAX_SIZE>::end(ns); }



#ifdef GENERATOR_MAIN
int main()
{
    auto ns = *(++Numbers_iterator<MIN_SIZE,MAX_SIZE>::begin(4));
    std::cout << "numbers: " << ns << std::endl;
    Size inf{2,2};    // todo: take max size into account in the iterator version
    auto bcombs_old = block_combinations(ns, inf);

    auto bcombs_new = std::vector<std::vector<Block>>{};

    // auto end_ = Block_combination_iterator<MIN_SIZE,MAX_SIZE>::end(ns);
    // for (auto b_it = Block_combination_iterator<MIN_SIZE,MAX_SIZE>::begin(ns);
    auto end_ = end(ns);
    for (auto b_it = begin(ns);
            b_it != end_;
            ++b_it)
        bcombs_new.push_back(*b_it);


    std::cout << "old size: " << bcombs_old.size() << std::endl;
    // for (auto ns : bcombs_old)
    //     std::cout << ns << std::endl;
    // std::cout << std::endl;

    std::cout << "new size: " << bcombs_new.size() << std::endl;
    // for (auto ns : bcombs_new)
    //     std::cout << ns << std::endl;
    // std::cout << std::endl;

}
#endif