#ifndef BLOCK_H
#define BLOCK_H

#include <iostream>
#include <vector>
#include <initializer_list>

struct Offset {
	int x{0};
	int y{0};
	Offset() {}
	Offset(int x, int y) : x{x}, y{y} {}
	Offset(unsigned x, unsigned y) : x{static_cast<int>(x)}, y{static_cast<int>(y)} {}
	Offset(const Offset&) = default;
	Offset& operator+=(const Offset& rhs) { x+=rhs.x; y+=rhs.y; return *this; }
	Offset& operator-=(const Offset& rhs) { x-=rhs.x; y-=rhs.y; return *this; }
	Offset operator+(const Offset& rhs) const { Offset p{rhs}; return p+=*this; }
	Offset operator-(const Offset& rhs) const { Offset p{rhs}; return p-=*this; }
	Offset operator-() const { return Offset(-x,-y); }
	bool operator==(const Offset& other) const { return x == other.x && y == other.y; }
	bool operator!=(const Offset& other) const { return !(*this==other); }
};

struct Position {
	int x{0};
	int y{0};
	Position() {}
	Position(int x, int y) : x{x}, y{y} {}
	Position(unsigned x, unsigned y) : x{static_cast<int>(x)}, y{static_cast<int>(y)} {}
	Position(const Position&) = default;
	Position(const Offset& o) : x{o.x}, y{o.y} {}	// the same as Position{}+=o;
	Position& operator+=(const Offset& rhs) { x+=rhs.x; y+=rhs.y; return *this; }
	Position& operator-=(const Offset& rhs) { x-=rhs.x; y-=rhs.y; return *this; }
	Position operator+(const Offset& rhs) const { return Position(*this)+=rhs; }
	Position operator-(const Offset& rhs) const { return Position(*this)-=rhs; }
	Offset operator-(const Position& rhs) const { return Offset(x-rhs.x,y-rhs.y); }
	bool operator==(const Position& other) const { return x == other.x && y == other.y; }
	bool operator!=(const Position& other) const { return !(*this==other); }
};

struct Size {
	unsigned width{};
	unsigned height{};
	unsigned get_area() const { return width * height; }
	bool operator==(const Size& other) const { return width == other.width && height == other.height; }
	bool operator!=(const Size& other) const { return !(*this==other); }
};

inline std::ostream& operator<<(std::ostream& os, const Size& size) { return os << size.width << "x" << size.height; }
inline std::ostream& operator<<(std::ostream& os, const Position& pos) { return os << pos.x << "," << pos.y; }
inline std::ostream& operator<<(std::ostream& os, const Offset& off) { return os << off.x << "," << off.y; }


class Block_iterator;

class Block {

public:

	Offset offset{};
	Size size {};

	// TODO: prune size
	explicit Block(const Size& size, const std::initializer_list<std::vector<bool>>& initializer_list = {});
	Block() {}

	Block rotate_counter_clockwise();
	Block rotate_clockwise();
	Block rotate_180();
	Block mirror_horizontal();
	Block mirror_vertical();

	Block(const Block& block) = default;

	unsigned get_area() const;
	bool overlaps(const Block& b) const;
	bool inside(const Position& p) const { return p.x>=0 && p.y>=0 && p.x<size.width && p.y<size.height; }

	bool get(int x, int y) const { return inside(Position{x,y}) && data_[y*size.width+x]; }
	bool get(Position p) const { return inside(p) && data_[p.y*size.width+p.x]; }

    virtual void draw_ascii(std::ostream&) const;
    friend std::istream& operator>>(std::istream&, Block&);	// has to have access to either set() or data_

    bool operator==(const Block& other) const { return size == other.size && offset == other.offset && data_ == other.data_; }
    bool operator!=(const Block& other) const { return !(*this==other); }

private:
	std::vector<bool> data_{};
	bool get_unchecked(int x, int y) const { return data_[y*size.width+x]; }
	bool get_unchecked(size_t x, size_t y) const { return data_[y*size.width+x]; }
	bool get_unchecked(Position p) const { return data_[p.y*size.width+p.x]; }

protected:
	void set(int x, int y, bool b) { data_[y*size.width+x] = b; }
	void set(Position p, bool b) { data_[p.y*size.width+p.x] = b; }

};

std::ostream& operator<<(std::ostream& os, const Block& block);







class Block_iterator {
public:
	Block_iterator(const Block& block, int i = 0) : block_ref{block}, i{i} {}
	void operator++() { i++; }
	Position operator*() { return Position{i%block_ref.size.width,i/block_ref.size.width}; }
	bool operator!=(const Block_iterator& rhs) { return i!=rhs.i; }

private:
	const Block& block_ref;
	int i{0};
};

inline Block_iterator begin(const Block& block) { return Block_iterator(block); }
inline Block_iterator end(const Block& block) { return Block_iterator(block, block.size.get_area()); }








// block_of_size gives a vector containin all legal blocks of that size.
// A legal block of size n consists of n tiles that are all adjacent.
// No "floating" pieces.

std::vector<Block> blocks_of_size(int n);
int number_of_blocks_of_size(int n);

#endif