#include "windows_thread.h"
#include <Windows.h>    // windows native defines
#include <vector>       // join_multiple with a vector of threads
#include <functional>   // lambdas

#include <iostream>     // debug output
#include <system_error> // errors for detatch()
#include <process.h>    // beginthread

// concept:
// LPTHREAD_START_ROUTINE  = function that takes a pointer to a lambda as a parameter and executes it



unsigned start_routine(void* f)
{
    try
    {
        auto& fun = *static_cast<std::function<void()>*>(f);
        if (fun == nullptr) std::cout << "fun has no target!" << std::endl;
        else (fun)();
        _endthreadex(0);
    }
    catch (std::bad_function_call)
    {
        std::cout << "caught bad function call!" << std::endl;
        _endthreadex(0);
    }
}

Windows_thread::~Windows_thread()
{
    if (joinable()) join();
    CloseHandle(handle);
}

Windows_thread::Windows_thread(const std::function<void()>& fun)
    : function{fun}
{
    start(&start_routine,&function);
}


void Windows_thread::start(unsigned (*fun)(void*), LPVOID arg)
{
    handle = (HANDLE)_beginthreadex(NULL, 0, fun, arg, 0, &thread_id);
    started = true;
}


Windows_thread& Windows_thread::operator=(Windows_thread&& other)
{
    if (started) {
        DWORD exit_code;
        GetExitCodeThread(handle,&exit_code);
        TerminateThread(handle,exit_code);
        CloseHandle(handle);
    }
    started = other.started;
    thread_id = other.thread_id;
    function = other.function;
    other.started = false;
}




void Windows_thread::detatch()
{
    if (!joinable()) throw std::system_error{std::error_code{}, "Cannot detatch non-joinable thread!"};
    started = false;
    thread_id = 0;
}


void Windows_thread::swap(Windows_thread& other)
{
    auto s = started;
    auto tid = thread_id;
    auto fun = function;

    started = other.started;
    thread_id = other.thread_id;
    function = other.function;

    other.started = s;
    other.thread_id = tid;
    other.function = fun;
}





// single thread join
void Windows_thread::join(int dwMilliseconds)
{
    WaitForSingleObject(native_handle(), dwMilliseconds);
}

void join(HANDLE handle, int dwMilliseconds)
{
    WaitForSingleObject(handle, dwMilliseconds);
}

void join(Windows_thread& thread, int dwMilliseconds)
{
    thread.join(dwMilliseconds);
}



// multiple thread join
void join_multiple(int count, const HANDLE* handles, bool wait_all, int dwMilliseconds)
{
    WaitForMultipleObjects(count, handles, wait_all, dwMilliseconds);
}

void join_multiple(int count, const Windows_thread* threads, bool wait_all, int dwMilliseconds)
{
    HANDLE handles[count];
    for (int i = 0; i < count; i++)
        handles[i] = threads[i].native_handle();
    join_multiple(count, handles, wait_all, dwMilliseconds);
}

void join_multiple(const std::vector<Windows_thread> threads, bool wait_all, int dwMilliseconds)
{
    int count = threads.size();
    HANDLE handles[count];
    for (int i = 0; i < count; i++)
        handles[i] = threads[i].native_handle();
    join_multiple(count, handles, wait_all, dwMilliseconds);
}


